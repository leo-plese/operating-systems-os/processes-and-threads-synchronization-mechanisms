Processes and Threads Synchronization Mechanisms. Tasks:

1 Process synchronization with semaphores ("vrtuljak.c")

2 Thread synchronization with monitor ("Nfilozofa.c")

Implemented in C using <pthread.h>, <signal.h>, <sys/types.h>, <sys/ipc.h>, <sys/sem.h> and <sys/wait.h> header files.

My lab assignment in Operating Systems, FER, Zagreb.

Task descriptions in "TaskSpecification_Vrtuljak.pdf" and "TaskSpecification_NFilozofa.pdf".

Created: 2018
