#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>

int semVrtuljakId;
int semSjeoId;
int semSisaoId;
int semKrajId;

int N;		//kapacitet vrtuljka (max br posjetitelja)

int sem_get(int n)
{
	int semId = semget(IPC_PRIVATE, n, 0600);
	if (semId == -1) {
		perror("Greska pri stvaranju semafora!\n");
		exit(1);
	}
	return semId;
}

void stvori_semafore()
{
	semVrtuljakId = sem_get(1);
	semSjeoId = sem_get(1);
	semSisaoId = sem_get(1);
	semKrajId = sem_get(1);
}

int sem_set_val(int semId, int semNum, int semVal)
{
	return semctl(semId, semNum, SETVAL, semVal);
}

void inicijaliziraj_semafore(int n)
{
	sem_set_val(semVrtuljakId, 0, 0);
	sem_set_val(semSjeoId, 0, 0);
	sem_set_val(semSisaoId, 0, 0);
	sem_set_val(semKrajId, 0, 0);
}

int sem_op(int semId, int semNum, int semOp)
{
	struct sembuf semBuf;
	semBuf.sem_num = semNum;
	semBuf.sem_op = semOp;
	semBuf.sem_flg = 0;
	return semop(semId, &semBuf, 1);
}

void sem_remove(int semId)
{
	semctl(semId, 0, IPC_RMID, 0);
}

void brisi(int sig)
{
	sem_remove(semVrtuljakId);
	sem_remove(semSjeoId);
	sem_remove(semSisaoId);
	sem_remove(semKrajId);
	exit(0);
}

void posjetitelj()
{
	//cekaj prazno mjesto na vrtuljku
	sem_op(semVrtuljakId, 0, -1);
	//sjedi
	printf("sjeo\n");
	//javi vrtuljku da si sjeo
	sem_op(semSjeoId, 0, +1);
	sleep(1);
	//cekaj kraj voznjes
	sem_op(semKrajId, 0, -1);
	//sidji
	printf("sisao\n");
	//javi vrtuljku da si sisao
	sem_op(semSisaoId, 0, +1);
	sleep(1);
}

int main(int argc, char *argv[])
{
	int i, k, brVoznje = 0;
	N = atoi(argv[1]);

	if (N <= 0) {
		printf("Broj posjetitelja N mora biti pozitivan!\n");
		return 1;
	}

	//stvoriti potrebne semafore
	stvori_semafore();

	//pocetno sve semafore inicijalizirati na 0
	inicijaliziraj_semafore(0);
	
	//unistavanje semafora i kraj programa na prekid SIGINT
	sigset(SIGINT, brisi);

	//vrtuljak
	while (1) {
		printf("%d. voznja\n", ++brVoznje);

		//stvaranje N procesa posjetitelja
		for (i = 0; i < N; i++) {
			switch (fork()) {
				case 0:
					posjetitelj();
					exit(0);
				case -1:	//greska
					perror("Greska pri stvaranju procesa!\n");
					exit(1);
			}
		}


		//postavi da na vrtuljku pocetno ima N slobodnih mjesta
		sem_op(semVrtuljakId, 0, +N);
		sleep(1);
		//cekaj da svi posjetitelji sjednu tj. da se vrtuljak napuni do svog kapaciteta
		sem_op(semSjeoId, 0, -N);		
		//voznja
		//svi su sjeli - pokreni vrtuljak
		printf("pokreni vrtuljak\n");
		//kraj voznje - zaustavi vrtuljak
		printf("zaustavi vrtuljak\n");
		
		
		//javi svim posjetiteljima da je voznja gotova
		sem_op(semKrajId, 0, +N);
		sleep(1);
		//cekaj da svi posjetitelji sidju tj. da se vrtuljak isprazni
		sem_op(semSisaoId, 0, -N);

		//cekanje vrtuljka da zavrse svih N procesa posjetitelja
		while (i--)
			wait(NULL);
	}

	return 0;
}
