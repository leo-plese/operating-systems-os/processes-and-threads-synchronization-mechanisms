#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

int Di = 0;	//oznaka trenutne dretve

int brCiklusa;	//broj ciklusa misliti-jesti kroz koje svaki filozof prolazi tijekom konferencije
int N;		//broj filozofa
int broj_stapica;	//ukupan broj stapica (sredstava)
int *ruke;	//broj ruku = potrebnih sredstava filozofa

pthread_mutex_t mutex;	//monitor
pthread_cond_t red;	//red uvjeta (dretve koje cekaju na potrebne resurse)


void uci_u_monitor()
{
	pthread_mutex_lock(&mutex);
}

void izaci_iz_monitora()
{
	pthread_mutex_unlock(&mutex);
}

void cekati_u_redu_uvjeta()
{
	pthread_cond_wait(&red, &mutex);
}

void osloboditi_sve_iz_reda_uvjeta()
{
	pthread_cond_broadcast(&red);
}

void uzeti_stapice(int f)
{
	uci_u_monitor();

	while (broj_stapica < ruke[f])
		cekati_u_redu_uvjeta();

	broj_stapica -= ruke[f];
	printf("F%d uzeo %d stapica -> %d stapica\n", f, ruke[f], broj_stapica);

	izaci_iz_monitora();
	sleep(1);
}

void spustiti_stapice(int f)
{
	uci_u_monitor();

	broj_stapica += ruke[f];
	printf("F%d oslobodio %d stapica -> %d stapica\n", f, ruke[f], broj_stapica);
	
	osloboditi_sve_iz_reda_uvjeta();

	izaci_iz_monitora();
	sleep(1);
}

void *zivot_filozofa(void *arg)
{
	int i;
	int dretva = Di++;

	for (i = 0; i < brCiklusa; i++) {
		printf("misliti %d\n", dretva);
		uzeti_stapice(dretva);
		printf("jesti %d\n", dretva);
		spustiti_stapice(dretva);
	}
}

int main(int argc, char *argv[])
{
	int i, brFil;
	pthread_t *thread_id;
	FILE *filDat;

	if ((brCiklusa = atoi(argv[1])) < 1) {
		printf("Broj ciklusa mora biti veci od 0!\n");
		return 0;
	}

	if ((filDat = fopen("filozofiData.txt", "r")) == NULL) {
		perror("Greska pri otvaranju datoteke!\n");
		exit(1);
	}

	if (fscanf(filDat, "%d %d\n", &broj_stapica, &N) == EOF) {
		perror("Greska pri citanju broja stapica i/ili broja filozofa!\n");
		exit(1);
	}

	ruke = (int *) malloc(N * sizeof(int));
	brFil = 0;
	while (fscanf(filDat, "%d\n", &ruke[brFil]) != EOF) {		
		if (ruke[brFil] < 2) {
			printf("Rasa filozofa F%d s %d ruku ne postoji.\n", brFil, ruke[brFil]);
			ruke[brFil] = 0; 	//filozof ce samo prolaziti kroz cikluse misliti-jesti, ali nece mijenjati broj_stapica
		} else if (ruke[brFil] > broj_stapica) {
			printf("Filozof F%d ima %d ruku sto je vise od ukupnog broja stapica %d.\n", brFil, ruke[brFil], broj_stapica);
			ruke[brFil] = 0; 	//filozof ce samo prolaziti kroz cikluse misliti-jesti, ali nece mijenjati broj_stapica
		}
		brFil++;
	}

	if (brFil < N) {
		printf("U datoteci nije dan podatak o broju ruku za sve filozofe!\n");
		exit(1);
	}

	fclose(filDat);


	thread_id = (pthread_t *) malloc(N * sizeof(pthread_t));
	
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&red, NULL);

	for (i = 0; i < N; i++) {
		if (pthread_create(&thread_id[i], NULL, zivot_filozofa, NULL) != 0) {
			perror("Greska pri stvaranju dretve!\n");
			exit(1);		
		} else {
			printf("Kreirana D%d.\n", i);		
		}
	}

	for (i = 0; i < N; i++)
		pthread_join(thread_id[i], NULL);

	free(ruke);
	free(thread_id);

	return 0;
}
